//
//  ISSTrackViewController.swift
//  ISSTrack
//
//  Created by Gowrie Sammandhamoorthy on 11/28/17.
//  Copyright © 2017 Gowrie Sammandhamoorthy. All rights reserved.
//

import UIKit
import MapKit

class ISSTrackViewController: UIViewController, CLLocationManagerDelegate, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableVw: UITableView!
    var locationManager = CLLocationManager()
    var currentLocation = CLLocation()
    var dataDic:[ISSTrackMappable] = []
    var topSpace:CGFloat = 0.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //Request authorization from the user to access the location.
        locationManager.requestAlwaysAuthorization()
        //Request authorization from the user to use in foreground
        locationManager.requestWhenInUseAuthorization()
        
        locationManager.delegate = self
        locationManager.startUpdatingLocation()
        
        //Nav height
    
        if let navController = self.navigationController {
            topSpace = UIApplication.shared.statusBarFrame.size.height + navController.navigationBar.frame.height
        }
        
    }

    //MARK: CLLocationManagerDelegate methods.
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        getUserLocation(status: status, manager: manager)
    }
    
    private func getUserLocation(status:CLAuthorizationStatus,manager:CLLocationManager) {
        if status == .authorizedWhenInUse || status == .authorizedAlways {
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
            if let locValueUnwrapped: CLLocationCoordinate2D = manager.location?.coordinate{
                #if DEBUG
                    print("locations = \(locValueUnwrapped.latitude) \(locValueUnwrapped.longitude)")
                #endif
                ISSTrackWebServiceManager.getISSData(latitude: String(locValueUnwrapped.latitude), longitude: String(locValueUnwrapped.longitude), completionHandler: {(locationList:Any?)-> Void in
                    if let locationListUnwrapped = locationList as? ISSTrackList{
                        self.dataDic = locationListUnwrapped.locationList
                        self.tableVw?.reloadData()
                    }
                })
            }
        }
    }
    
    //MARK: UITableView Datasource and Delegate methods.
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataDic.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as? ISSTrackerTableViewCell
        guard let unwrappedCell = cell else{
            return .init(style: .default, reuseIdentifier: "cell")
        }
        let value:ISSTrackMappable = dataDic[indexPath.row]
        unwrappedCell.durationLabel?.text =  value.duration
        unwrappedCell.riseTimeLabel?.text = value.riseTime
        
        //Apply cell styles.
        applyCellStyles(cell: unwrappedCell, indexPath: indexPath)
        
        //Enable scroll
        tableView.isScrollEnabled = (tableView.frame.size.height > view.frame.size.height - topSpace) ? true : false
        
        return unwrappedCell
    }
    
    //MARK: Cell styles.
    func applyCellStyles(cell:UITableViewCell, indexPath:IndexPath) {
        tableVw?.separatorStyle = .singleLine
        tableVw?.separatorColor = .lightGray
        tableVw?.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0)
        self.tableVw?.sizeToFit()
    }
    
    //Cancel Bar button- will exit the application.
    @IBAction func cancelBarButtonTapped(_ sender: Any) {
        exit(0)
    }

}
