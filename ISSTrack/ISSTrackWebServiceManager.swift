//
//  ISSTrackWebServiceManager.swift
//  ISSTrack
//
//  Created by Gowrie Sammandhamoorthy on 11/28/17.
//  Copyright © 2017 Gowrie Sammandhamoorthy. All rights reserved.
//

import Foundation


struct ISSTrackWebServiceManager {
    
    //This method will create api url with latitude and longitude.
    static func url(lat:String, lon:String) -> URL {
        return URL(string: "http://api.open-notify.org/iss-pass.json?lat=\(lat)&lon=\(lon)")!
    }
    
    //Get ISS data by sending user's location.
    static func getISSData(latitude:String, longitude:String, completionHandler:@escaping ((_ locationList:Any?)->Void)) {
        //create the url with argument.
        let url = self.url(lat:latitude, lon:longitude)
        #if DEBUG
            print("Base Url: \(url)")
        #endif
        //create the session object
        let session = URLSession.shared
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            guard error == nil else {
                #if DEBUG
                    print("Response: Error occured: \(String(describing:error))")
                #endif
                return
            }
            guard let data = data else {
                #if DEBUG
                    print("Response: response object is empty")
                #endif
                return
            }
            do {
                //Parse json object.
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                    #if DEBUG
                        print("Response: \(json)")
                    #endif
                    if let responseUnwrapped = ISSTrackList.Map(json){
                        DispatchQueue.main.async {
                            completionHandler(responseUnwrapped)
                        }
                    }
                }
            } catch let error {
                #if DEBUG
                    print("Error:\(error.localizedDescription)")
                #endif
            }
        })
        task.resume()
    }
}
