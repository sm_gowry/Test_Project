//
//  ISSTrackerTableViewCell.swift
//  ISSTrack
//
//  Created by Gowrie Sammandhamoorthy on 11/28/17.
//  Copyright © 2017 Gowrie Sammandhamoorthy. All rights reserved.
//

import UIKit

class ISSTrackerTableViewCell: UITableViewCell {

    @IBOutlet weak var riseTimeLabel: UILabel!
    @IBOutlet weak var durationLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle  = .none
    }

}
