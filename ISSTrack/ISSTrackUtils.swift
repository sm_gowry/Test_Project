//
//  ISSTrackUtils.swift
//  ISSTrack
//
//  Created by Gowrie Sammandhamoorthy on 11/28/17.
//  Copyright © 2017 Gowrie Sammandhamoorthy. All rights reserved.
//

import Foundation

struct ISSTrackUtils {
    
    //MARK: JSON data serializing.
    /* Decodes value for Dictionary data
     - parameter key: The key for value to be extracted
     - parameter object: The dictionary to parsed
     */
    static func decodeData(_ key: String, object: Any) -> Any {
        if let dictionaryToParse = object as? [String:Any] {
            return dictionaryToParse[key] ?? ""
        }
        return ""
    }
    
    //MARK: Convert Rise time.
    /*
     This methods convert the Unix timestamp into the readable format.
     */
    static func convertRiseTimeToHumanReadableTime(riseTime:Int) -> String {
        let humanRiseTime:Date = Date(timeIntervalSince1970: Double(riseTime))
        let dateFormatter = DateFormatter()
        /*
         - Format - Month:date:year hour:Minute:Second "MMM dd YYYY hh:mm:ss a".
         */
        dateFormatter.dateFormat = "MMM dd YYYY hh:mm:ss a"
        return dateFormatter.string(from: humanRiseTime)
    }
}
