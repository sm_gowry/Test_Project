//
//  ISSTrackModal.swift
//  ISSTrack
//
//  Created by Gowrie Sammandhamoorthy on 11/28/17.
//  Copyright © 2017 Gowrie Sammandhamoorthy. All rights reserved.
//

import Foundation

/*
 This modal will parse only risetime and duration.
  - Coverts and returns riseTime and duration in string.
 */
struct ISSTrackMappable {
    var duration: String
    var riseTime: String
    
    static func Map(_ json: [String:Any]?) -> ISSTrackMappable? {
        if let dict = json {
            let riseTime = ISSTrackUtils.decodeData(String("risetime"), object: dict) as? Int ?? 0
            //Convert to readable time format.
            let humanRiseTime:String = ISSTrackUtils.convertRiseTimeToHumanReadableTime(riseTime: riseTime)
            let duration:Int = ISSTrackUtils.decodeData(String("duration"), object: dict) as? Int ?? 0
            return ISSTrackMappable(duration: String(describing:duration), riseTime: humanRiseTime)
        }
        return nil
    }}

/*
 Parse response and create array of object which contain
 - Coverts and returns riseTime and duration in string.
 - This contain array of objects which contain rise time and duraion.
 - Can access like ISSTrackList.locationList[Object Numer].duration or ISSTrackList.locationList[Object Numer].risetime
 */
struct ISSTrackList {
    let locationList : [ISSTrackMappable]
    static func Map(_ json: [String: Any]?) -> ISSTrackList? {
        if let map = json {
            let locationList:[Any] = ISSTrackUtils.decodeData(String("response"), object: map) as? [Any] ?? []
            var list:[ISSTrackMappable] = []
            for i in locationList{
                if let objectUnwrapped = i as? [String:Any] {
                    list.append(ISSTrackMappable.Map(objectUnwrapped)!)
                }
            }
            return ISSTrackList(locationList: list )
        }
        return nil
    }
}
