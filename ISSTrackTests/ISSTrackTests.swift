//
//  ISSTrackTests.swift
//  ISSTrackTests
//
//  Created by Gowrie Sammandhamoorthy on 11/28/17.
//  Copyright © 2017 Gowrie Sammandhamoorthy. All rights reserved.
//

import XCTest
@testable import ISSTrack

class ISSTrackTests: XCTestCase {
    
    var jsonObject:[String:Any] = [:]
    var dataDic:[ISSTrackMappable] = []
    var firstObject:ISSTrackMappable?
    var storyBoard:UIStoryboard?
    var vc: ISSTrackViewController?
    
    override func setUp() {
        super.setUp()
        
        //TO DO: Good to move this to a .json file and pull to test without adding in class.
        jsonObject = [ "message": "success",
                       "request": [
                        "altitude": 100,
                        "datetime": 1511814896,
                        "latitude": 37.33233141,
                        "longitude": -122.0312186,
                        "passes": 5
            ],
                       "response": [
                        ["duration": 307,
                         "risetime": 1511827855
                        ],[
                            "duration": 633,
                            "risetime": 1511833437
                        ],[
                            "duration": 593,
                            "risetime": 1511839256
                        ],[
                            "duration": 472,
                            "risetime": 1511845168
                        ],[
                            "duration": 494,
                            "risetime": 1511851028
                        ]]
        ]
        
        if let bundle = Bundle.main.bundleIdentifier{
            storyBoard = UIStoryboard(name: "Main", bundle: Bundle(identifier:bundle))
        }
        guard let vwController = storyBoard?.instantiateViewController(withIdentifier: "ISSTrackVC") as? ISSTrackViewController else{
            XCTAssert(false, "ISSTrackViewController doesn't have identifier ISSTrackVC")
            return
        }
        vc = vwController
        vc?.loadView()
        vc?.viewDidLoad()
    }
    
    //Test Mappable - Data Parsing
    func testMappableList() {
        let arrayOfObject = ISSTrackList.Map(jsonObject)
        
        guard let locationListUnwrapped:[ISSTrackMappable] = arrayOfObject?.locationList else{
            XCTAssert(false, "locationList is nil")
         return
        }
        dataDic = locationListUnwrapped
        XCTAssertEqual(locationListUnwrapped.count, 5, "CBMMappableList not returning correct number of response object")
        firstObject = locationListUnwrapped[0]
        XCTAssertEqual(firstObject?.duration, "307", "CBMMappableList not returning correct number of response object")
        XCTAssertEqual(firstObject?.riseTime, "Nov 27 2017 06:10:55 PM", "CBMMappableList not returning correct number of response object")
    }
    
    //Test unix timestamp converter
    func testConvertRiseTimeToHumanReadableTime() {
        XCTAssertEqual(ISSTrackUtils.convertRiseTimeToHumanReadableTime(riseTime:1511827855),"Nov 27 2017 06:10:55 PM", "Rise time convert value is wrong" )
    }
    
    //MARK: Test ISSTrackViewController
    func testOutlets() {
        guard let vcUnwrapped = vc else {
            XCTAssert(false, "View Controller is nil")
            return
        }
        XCTAssertNotNil(vcUnwrapped.tableVw,"Table view not conneted properly")
    }
    
    //MARK: Test View delagate and datasource test
    func testTableViewDelegateMethods() {
        guard let vcUnwrapped = vc else {
            XCTAssert(false, "View Controller is nil")
            return
        }
        let arrayOfObject = ISSTrackList.Map(jsonObject)
        
        guard let locationListUnwrapped:[ISSTrackMappable] = arrayOfObject?.locationList else{
            XCTAssert(false, "locationList is nil")
            return
        }
        dataDic = locationListUnwrapped
        vcUnwrapped.dataDic = dataDic
        
        let indexpath = IndexPath(row: 0, section: 0)
        guard let cellUnwrapped = vcUnwrapped.tableView(vcUnwrapped.tableVw, cellForRowAt: indexpath) as? ISSTrackerTableViewCell else{
            XCTAssert(false, "cell is nil")
            return
        }
        XCTAssertNotNil(vcUnwrapped.applyCellStyles(cell: cellUnwrapped, indexPath: indexpath),"applyCellStyles function not exist in ISSTrackViewController")
        XCTAssertEqual(vcUnwrapped.tableVw.numberOfSections, 1,"Table view number of section is more or less than 1.")
        XCTAssertEqual(vcUnwrapped.tableVw.numberOfRows(inSection: 0), dataDic.count,"Table view number of section is more or less than 1.")
        XCTAssertEqual(vcUnwrapped.tableVw.separatorInset, UIEdgeInsetsMake(0, 0, 0, 0),"Table view cell sepator not set to edge")
        XCTAssertEqual(vcUnwrapped.tableVw.separatorStyle, .singleLine,"Table view cell sepator style not set to singleLine")
        XCTAssertEqual(vcUnwrapped.tableVw.separatorColor, .lightGray,"Table view cell sepator color not set to lightGray")
        XCTAssertEqual(cellUnwrapped.selectionStyle, .none,"Table view number of selection style is not none.")
        XCTAssertEqual(cellUnwrapped.durationLabel?.text, "307" , "Data not displaying in correct order.")
        XCTAssertEqual(cellUnwrapped.riseTimeLabel?.text, "Nov 27 2017 06:10:55 PM" , "Data not displaying in correct order.")
    }

    func testMapKitDelegateMethods() {
        guard let vcUnwrapped = vc else {
            XCTAssert(false, "View Controller is nil")
            return
        }
        XCTAssertNotNil(vcUnwrapped.locationManager(vcUnwrapped.locationManager, didChangeAuthorization: .authorizedAlways), "didChangeAuthorization delegate method not exist in ISSTrackViewController")
    }
    
    override func tearDown() {
        jsonObject = [:]
        dataDic = []
        firstObject = nil
        vc = nil
        storyBoard = nil
        super.tearDown()
    }
    
}
